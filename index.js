const express = require('express');
const morgan = require('morgan');

const app = express();
const { PORT = 8000 } = process.env;

const books = [];

app.use(express.json());
app.use(morgan('tiny'));

function generateBookId() {
  const temporaryBooks = books.sort((a, b) => a.id - b.id);
  return (temporaryBooks[temporaryBooks.length - 1].id || 0) + 1;
}

function handlePostBooks(req, res) {
  const id = generateBookId();
  const book = {
    id,
    title: req.body?.title,
    synopsis: req.body?.synopsis,
    author: req.body?.author,
    publisher: req.body?.publisher,
  };

  books.push(book);

  res.status(201).json({
    status: 'OK',
    data: book,
  });
}

function handleGetRoot(req, res) {
  res.status(200).json({
    status: 'OK',
    data: null,
  });
}

app.get('/', handleGetRoot);
app.post('/api/v1/books', handlePostBooks);

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`);
});
