const fruits = [
  {
    name: 'Banana',
    price: 5000,
  },
  {
    name: 'Apple',
    price: 5000,
  },
  {
    name: 'Orange',
    price: 8000,
  },
  {
    name: 'Mango',
    price: 10000,
  },
];

const filteredFruits = fruits
  .filter((object) => object.price <= 5000)
  .map((object) => object.name);

console.log(filteredFruits);
